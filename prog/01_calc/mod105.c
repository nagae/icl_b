#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a, b, c;			/* 整数型の変数 a, b, c を宣言 */
  /* x を 3, 5, 7 で割った余りを順に入力させる */
  printf("x を 3 で割った余りを入力して下さい\n");
  scanf("%d", &a);
  printf("x を 5 で割った余りを入力して下さい\n");
  scanf("%d", &b);
  printf("x を 7 で割った余りを入力して下さい\n");
  scanf("%d", &c);

  /* 百五減算で x を求めて表示する */
  int x = ( 70 * a + 21 * b + 15 * c) % 105;
  printf("xを3,5,7で割った余りがそれぞれ%d,%d,%dなので\n", a, b, c);
  printf("x は %d ですね\n", x);

  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
