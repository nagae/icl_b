#include <stdio.h>	      /* 標準入出力ライブラリ */
int main（void)		      /* main 関数の引数と戻り値の定義 */
{			      /* main 関数の始まり */
　printf("Hello world\n");    /* printf 関数の呼び出し */
　return　0;　		      /* 戻り値として 0 を返す */
｝			      /* main 関数の終わり */
