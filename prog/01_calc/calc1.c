#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a;			/* 整数型の変数 a を宣言*/
  a = 3;			/* aに3を代入 */
  printf("aの値は%dです\n", a);	/* aの値を出力 */
  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
