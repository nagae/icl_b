#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a, b;			/* 整数型の変数 a, b を宣言 */
  printf("2つの整数を入力して下さい\n");
  scanf("%d%d", &a, &b);	/* キーボードからaとbに値を読込む */

  printf("a=%d, b=%d\n", a, b);	/* aとbの値を表示 */
  printf("差:%d\n", a-b);	/* 差を表示 */
  printf("商:%d, 剰余:%d\n", a/b, a%b); /* 商と剰余を表示 */

  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
  
