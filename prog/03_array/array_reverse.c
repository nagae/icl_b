#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int N = 4;			/* 入力する整数の数(この場合4)を N に格納 */
  int a[N];			/* N個の要素を持つ整数型配列 a を定義 */

  int i;			/* 要素番号を格納するための変数 */
  
  /* N個の整数を読込む */
  printf("%d個の整数? ", N);
  for (i = 0; i < N; ++i)	/* iを0からN-1まで1つづつ増やしながら繰り返し */
    { scanf("%d", &a[i]); }	/* 配列aのi番目要素 a[i] に値を読込む*/

  /* 入力されたのと逆の順に表示 */
  for (i = N-1; i >=0; --i)	/* iをN-1から0まで1つづ減らしながら繰り返し */
    { printf("%d ", a[i]); }	/* 配列aのi番目要素 a[i] を出力する */
  printf("\n");			/* 最後に改行を出力 */

  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
