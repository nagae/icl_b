#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a;			/* 整数型の変数 a を宣言 */
  scanf("%d", &a);		/* キーボードから a に値を読込む */

  if ( (a % 3 == 0) && (a % 5 == 0) ) /* 3でも5でも割り切れる時の処理 */
    { printf("Fizz Buzz\n"); }
  else if (a % 3 == 0)		/* 3で割り切れる時の処理 */
    { printf("Fizz\n"); }
  else if (a % 5 == 0)		/* 5で割り切れる時の処理 */
    { printf("Buzz\n"); }
  else				/* 3でも5でも割り切れないときの処理 */
    { printf("%d\n", a); }
  
  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
