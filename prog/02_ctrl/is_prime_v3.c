#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a;			/* 整数型の変数 a を宣言 */
  scanf("%d", &a);		/* キーボードから a に値を読込む */

  int is_prime = 1;		/* 素数の時に1となり, そうでなければ0となる変数 */

  /* a=1 の場合 */
  if (a == 1)
    { is_prime = 0; }		/* a=1なら素数ではないので is_prime を 0 にする */
  
  /* a>1 の場合 */
  int i;			/* 候補の数を順に格納するための変数 */
  for (i = 2; i*i <= a; ++i)	/* 約数の候補を2から順に1づつ増やしながら繰り返す */
    {				/* 繰り返しの始まり */
      if (a % i == 0)		/* aが i で割り切れるかを判定 */
	{			
	  is_prime = 0;		/* aが i で割り切れるなら is_prime を 0 にする */
	  break;
	}                       
    }				/* 繰り返しの終了 */
  
  if (is_prime == 1)	/* is_prime が 1ならば素数 */
    { printf("%dは素数です\n", a); }

  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
