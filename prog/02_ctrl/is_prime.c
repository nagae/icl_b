#include <stdio.h>		/* 標準入出力ライブラリ */
int main(void)			/* main 関数の引数と戻り値の定義 */
{				/* main 関数の始まり */
  int a;			/* 整数型の変数 a を宣言 */
  scanf("%d", &a);		/* キーボードから a に値を読込む */

  int i;			/* 候補の数を順に格納するための変数 */
  int divisor = 1;		/* 1より大きい約数を格納する変数 */
  for (i = 2; i < a; ++i)	/* 約数の候補を2から順に1づつ増やしながら繰り返す */
    {				/* 繰り返しの始まり */
      if (a % i == 0)		/* aが i で割り切れるかを判定 */
	{			
	  divisor = i;		/* aが i で割り切れるならそれを divisor として記憶*/
	}                       
    }				/* 繰り返しの終了 */
  
  if (divisor == 1 && a != divisor) /* 1より大きくaより小さい約数が無ければ素数 */
    { printf("%dは素数です\n", a); }
  else				/* 1より大きい約数があったら合成数と表示 */
    { printf("%d=%d*%d\n", a, divisor, a/divisor); }

  return 0;			/* 戻り値として 0 を返す */
}				/* main 関数の終わり */
